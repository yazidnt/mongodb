# Feeds Using Mongodb

## API
- [ ] create feed
  - endpoint: /api/feed
  - method: POST
  - payload:
  ```json
    {
         "created_at":"2022-05-12T02:31:44.317Z",
         "feed_type":"VISUAL_POST",
         "has_comment":false,
         "has_like":false,
         "_id":"8963cbff-4ad4-401f-82de-6be10bf8caa8",
         "message":"Memposting aktivitas.",
         "object_type":"POST",
         "post":{
            "id":"f733902f-e3d1-451e-ab94-f2a799f4dd3b",
            "image_url":null,
            "message":"Postingan yang dikhususkan untuk semua orang"
         },
         "sender":{
            "id":"bf89c203-c6b8-40dc-968b-851757722c69",
            "logo_url":"https://dev-storage-edoo.mocogawe.com/bf89c203-c6b8-40dc-968b-851757722c69/images/school-logo/bf89c203-c6b8-40dc-968b-851757722c69.jpg",
            "name":"Edoo"
         },
         "sender_type":"SCHOOL",
         "total_comment":0,
         "total_like":0,
         "updated_at":"2022-05-12T02:31:44.317Z"
    }
  ```

- [ ] update feed by id
  - endpoint: /api/feed/{id}
  - method: PUT
  - payload:
  ```json
    {
        "total_comment": 1,
        "has_comment": true
    }
  ```
- [ ] delete feed by id
  - endpoint: /api/feed/{id}
  - method: DELETE
- [ ] display feeds
  - endpoint: /api/feed
  - method: GET
  - limit: 10
  - offset: 0
  - response:
  ```json
    {
        "data":[
            {
                "created_at":"2022-05-12T02:31:44.317Z",
                "feed_type":"VISUAL_POST",
                "has_comment":false,
                "has_like":false,
                "_id":"8963cbff-4ad4-401f-82de-6be10bf8caa8",
                "message":"Memposting aktivitas.",
                "object_type":"POST",
                "post":{
                    "id":"f733902f-e3d1-451e-ab94-f2a799f4dd3b",
                    "image_url":null,
                    "message":"Postingan yang dikhususkan untuk semua orang"
                },
                "sender":{
                    "id":"bf89c203-c6b8-40dc-968b-851757722c69",
                    "logo_url":"https://dev-storage-edoo.mocogawe.com/bf89c203-c6b8-40dc-968b-851757722c69/images/school-logo/bf89c203-c6b8-40dc-968b-851757722c69.jpg",
                    "name":"Edoo"
                },
                "sender_type":"SCHOOL",
                "total_comment":0,
                "total_like":0,
                "updated_at":"2022-05-12T02:31:44.317Z"
            },
            {
                "book":{
                    "authors":"CakeMan",
                    "avg_rating":null,
                    "book_title":"What Even is Real?",
                    "cover_url":"https://dev-storage-edoo.mocogawe.com/3ee96d7a-b555-4c28-9a73-d862f9bcd35b/images/textbooks/0c0004c8-2ea9-4701-80ed-d190899c824f.jpg",
                    "id":"82cfd553-101e-4e0a-bdbb-511de3908d79"
                },
                "created_at":"2022-04-06T07:20:42.273Z",
                "feed_type":"BOOK_BORROW",
                "has_comment":false,
                "has_like":false,
                "_id":"cce5eb65-0aca-46a8-9124-3a36a9e24c25",
                "message":"Menambahkan buku",
                "object_type":"BOOK",
                "sender":{
                    "avatar_url":"https://dev-storage-edoo.mocogawe.com/bf89c203-c6b8-40dc-968b-851757722c69/images/avatars/f3103f5f-31bb-43e3-9dda-3fa8345d3db4.jpg",
                    "id":"f3103f5f-31bb-43e3-9dda-3fa8345d3db4",
                    "name":"Raden Ajeng Cokrodinoto"
                },
                "sender_type":"USER",
                "total_comment":0,
                "total_like":0,
                "updated_at":"2022-04-06T07:20:42.273Z"
            }
        ],
        "meta":{
            "from":60,
            "to":70,
            "limit":10,
            "offset":60
        }
    }
  ```
